
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
/* global $ */
/* global L */
var username = "ois.seminar";
var password = "ois4fri";
var ehr1;
var ehr2;
var ehr3;
var ehr4;
var stevec = 4;
var vredu = true;
var checked = true;
var istiEHR;

var map;
var bolnice = [];
var layers = [];

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
function generiranjePodatkov() {
  
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datum = $("#kreirajDatumRojstva").val();
    var visina = $("#kreirajVisino").val();
    var teza = $("#kreirajTezo").val();
    
    var vis = visina/100;
    var vis2 = vis*vis;
    
    var BMI = teza/vis2;
  
  
    var podatki = $("#kreirajDatumRojstva").val().split("-");
    var today = new Date();
    var dd = today.getDate()
    var mm = today.getMonth() + 1
    var yyyy = today.getFullYear(); 
  
    if (mm>podatki[1]){
       var age = yyyy - podatki[0];
    }
    else if (mm==podatki[1] && dd>=podatki[2]){
      var age = yyyy - podatki[0];
    }
    else {
      var age = yyyy - podatki[0] - 1;
    }
    
    if (document.getElementById("nic").selected){
      generirajPodatke(1);
      generirajPodatke(2);
      generirajPodatke(3);
      var element = document.getElementById("Marko");
      element.classList.remove("button");
      element = document.getElementById("SmallBoi");
      element.classList.remove("button");
      element = document.getElementById("BigBoi");
      element.classList.remove("button");
    }
    else if (!ime || !priimek || !datum || !visina || !teza || ime =="" || priimek=="" || datum=="" || visina=="" || teza==""){
    $("#kreirajSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" + "Najprej vstavite osebne podatke, nato generirajte EHR ID, z gumbom <strong>Generiranje podatkov</strong>'");
    }
    else if(BMI>60 ||BMI<0){
      $("#kreirajSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" + "ITM je višji od 60, tega spletna stran na žalost ne podpira, svetujemo obisk zdravnika'");
    }
    else if (age<18){
       $("#kreirajSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" + "Starost je manjša od 18, spletna stran tega na žalost ne podpira'");
    }
    else if (age>120){
       $("#kreirajSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" + "Preverite svoj datum rojstva, saj ste napisali, da ste stari " + age + " let'");
    }
    else if (document.getElementById("Marko").selected){
      generirajPodatke(1);
    }
    else if (document.getElementById("BigBoi").selected){
      generirajPodatke(2);
    }
    else if (document.getElementById("SmallBoi").selected){
      generirajPodatke(3);
    }
    else{
      generirajPodatke(4);
    }

    //alert("Osebe uspešno generirane");
}
 
function generirajPodatke(stPacienta) {
	var ime;
	var priimek;
  var datumRojstva;
  var visina;
  var teza;


  if (stPacienta==1){
      ime = "Janez";
      priimek = "Novak";
      datumRojstva = "1996-04-23";
      visina = 173;
      teza = 67;
    }
    else if (stPacienta==2){
      ime = "Ana";
      priimek = "Zidar";
      datumRojstva = "1965-05-11";
      visina = 190;
      teza = 120;
    }
    else if (stPacienta==3){
      ime = "Franc";
      priimek = "Krt";
      datumRojstva = "2000-02-29";
      visina = 154;
      teza = 40;
    }
    else if (stPacienta==4){
      ime = $("#kreirajIme").val();
      priimek = $("#kreirajPriimek").val();
      datumRojstva = $("#kreirajDatumRojstva").val();
      visina = $("#kreirajVisino").val();
      teza = $("#kreirajTezo").val();
    }
	
	$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
 
                        if(stPacienta == 1){
                           ehr1 = ehrId;
                        }
                        else if(stPacienta == 2){
                            ehr2 = ehrId;
                        }
                        else if(stPacienta == 3){
                            ehr3 = ehrId;
                        }
                        else if (stPacienta == 4){
                          ehr4 = ehrId;
                          istiEHR = ehrId;
                        }
                        
                        dodajPodatkeOsebe(ehrId,visina,teza, function neki(vredu){
                        if (vredu){
                          
                          var x = document.getElementById("preberiObstojeciEHR");
                          var option = document.createElement("option");
              
                         option.text = ime + " " + priimek;
                         option.value = ehrId;
                         x.add(option)
                        //document.getElementById("preberiPredlogoBolnika").options[stevec].selected = 'selected';
                          for (var i=0; i<document.getElementById("preberiObstojeciEHR").options.length; i++){
                            if (document.getElementById("preberiObstojeciEHR").options[i].value == option.value){
                              $("#preberiEHRid").val(document.getElementById("preberiObstojeciEHR").options[i].selected = "selected");
                            }
                           }
                          
                          alert("EHR ID osebe " + ime + " " + priimek + " je bil uspešno generiran");
                          //$("#kreirajEhrId").val(ehrId);
                          if (document.getElementById("nic").selected){
                            $("#kreirajEhrId").val("");
                            $("#preberiEHRid").val(ehrId);
                          }
                          else if (document.getElementById("Nov").selected){
                            $("#kreirajEhrId").val(ehrId);
                            $("#preberiEHRid").val(ehrId);
                          }
                          else {
                            $("#kreirajEhrId").val(ehrId);
                            //$("#preberiEHRid").val(ehrId);
                          }
                           $("#kreirajSporocilo").html("");
                         }
                        });
              
              console.log(ehrId);
              //istiEHR = ehrId;
              //dodajPodatkeOsebe(ehrId,visina,teza, vredu);
               //$("#kreirajEhrId").val(ehrId);
               //callback(ehrId);
            }
          },
          error: function(err) {
                    if (err) {
                        console.log(err);
                        alert("Napaka: Generiranje EHR ID");
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                         "label-danger fade-in'>Napaka '" + "Neustrezni podatki'");
                    }
                }
        });
        
      }
		});
}

function dodajPodatkeOsebe(ehrId, visina, teza, callback) {
    if (!ehrId || ehrId.trim().length == 0) {
		console.log("NE DELA");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "neki"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
            console.log("Dodana meritev");
            callback(true);
        },
        
        error: function(err) {
            if (err) {
                 console.log("Napaka: Dodajanje meritve");
                 console.log(err);
                 alert("Napaka: Generiranje EHR ID");
                        $("#kreirajSporocilo").html("<span class='obvestilo label " +
                         "label-danger fade-in'>Napaka '" + "Neustrezni podatki'");
                 callback(false);
            }
        }
		});
	}
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

$(document).ready(function() {
  
  $('#spol').change(function() {
    if (checked){
      checked=false;
    }
    else{
      checked=true;
    }
    ITM();
    graf();
  });
  
  /*$('#klikniITM').click(function() {
    document.getElementById("vrednostITM").innerHTML = 100;
  });*/
  
  /*function dodej() {
  var x = document.getElementById("preberiPredlogoBolnika");
  var option = document.createElement("option");
  option.text = "NEKI";
  x.add(option);
  }
  
  function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}*/

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiObstojeciEHR').change(function() {
    console.log(document.getElementById("Dejan").value);
    
     for (var i=0; i<document.getElementById("preberiObstojeciEHR").options.length; i++){
           if (document.getElementById("preberiObstojeciEHR").options[i].selected){
             $("#preberiEHRid").val(document.getElementById("preberiObstojeciEHR").options[i].value);
           }
      }
  });
  
  $('#preberiEHRid').keyup(function() {
      if ($('#preberiEHRid').val() == ""){
        document.getElementById("preberiObstojeciEHR").options[0].selected = "selected";
      }
  });
   
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    //console.log($("#kreirajIme").val(podatki[0]));
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajVisino").val(podatki[3]);
    $("#kreirajTezo").val(podatki[4]);
    
    if (document.getElementById("Marko").selected){
      $("#kreirajEhrId").val(ehr1);
      //$("#preberiEHRid").val(ehr1);
      if (checked == false){
       $('#spol').bootstrapToggle('on')   
       $('#spol').trigger('change');
      }
    }
    else if (document.getElementById("BigBoi").selected){
      $("#kreirajEhrId").val(ehr2);
      if (checked){
       $('#spol').bootstrapToggle('off')   
       $('#spol').trigger('change');
      }
      //$("#preberiEHRid").val(ehr2);
    }
    else if (document.getElementById("SmallBoi").selected){
      $("#kreirajEhrId").val(ehr3);
      //$("#preberiEHRid").val(ehr3);
      if (checked == false){
       $('#spol').bootstrapToggle('on')   
       $('#spol').trigger('change');
      }
    }
    else if (document.getElementById("nic").selected){
      $("#kreirajEhrId").val("");
      $("#kreirajIme").val("");
      //$("#preberiEHRid").val("");
    }
    else if(document.getElementById("Nov").selected){
       $("#kreirajIme").val("");
       $("#kreirajEhrId").val("");
       //$("#preberiEHRid").val("");
    }
    else {
      $("#kreirajEhrId").val(podatki[5]);
      //$("#preberiEHRid").val(podatki[5]);
    }
    
    if (document.getElementById("Nov").selected){
      var element = document.getElementById("preberiPredlogoBolnika");
      element.classList.remove("mystyle2");
      element.classList.add("mystyle");
      var element2 = document.getElementById("Gumb");
      element2.classList.remove("button");
      
      $("#kreirajPriimek")[0].disabled = false;
      $("#kreirajIme")[0].disabled = false;
      $("#kreirajTezo")[0].disabled = false;
      $("#kreirajVisino")[0].disabled = false;
      $("#kreirajEhrId")[0].disabled = false;
      $("#kreirajDatumRojstva")[0].disabled = false;
    }
    else if (document.getElementById("nic").selected){
      var element = document.getElementById("preberiPredlogoBolnika");
      element.classList.remove("mystyle");
      element.classList.add("mystyle2");
      
      $("#kreirajPriimek")[0].disabled = true;
      $("#kreirajIme")[0].disabled = true;
      $("#kreirajTezo")[0].disabled = true;
      $("#kreirajVisino")[0].disabled = true;
      $("#kreirajEhrId")[0].disabled = true;
      $("#kreirajDatumRojstva")[0].disabled = true;
      var element2 = document.getElementById("Gumb");
      element2.classList.add("button");
    }
    else {
      
      $("#kreirajPriimek")[0].disabled = true;
      $("#kreirajIme")[0].disabled = true;
      $("#kreirajTezo")[0].disabled = true;
      $("#kreirajVisino")[0].disabled = true;
      $("#kreirajEhrId")[0].disabled = true;
      $("#kreirajDatumRojstva")[0].disabled = true;
      
      var element = document.getElementById("preberiPredlogoBolnika");
      element.classList.remove("mystyle2");
      element.classList.remove("mystyle");
      var element2 = document.getElementById("Gumb");
      element2.classList.add("button");
    }
    ITM();
    graf();
  });

});

  function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();
	var najdu = false;
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  				var party = data.party;
  				console.log(party);
  				$("#preberiSporocilo").html("");
  				
           var podatki = $("#preberiPredlogoBolnika").val().split(",");
           console.log(document.getElementById("preberiPredlogoBolnika").options[1].text);
           console.log(party.lastNames + " " + party.firstNames);
           for (var i=0; i<document.getElementById("preberiPredlogoBolnika").options.length; i++){
             if (document.getElementById("preberiPredlogoBolnika").options[i].text == party.firstNames + " " + party.lastNames){
                document.getElementById("preberiPredlogoBolnika").options[i].selected = 'selected';
                najdu = true;
             }
           }
           if (najdu){
             $('#preberiPredlogoBolnika').trigger('change');
           }
           else {
            $("#kreirajIme").val(party.firstNames);
            $("#kreirajPriimek").val(party.lastNames);
            $("#kreirajDatumRojstva").val(party.dateOfBirth);
           }
            
            $.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			      //console.log(res[0].weight);
    			      $("#preberiSporocilo").html("");
    			      if (najdu == false){
                   $("#kreirajTezo").val(res[0].weight);
    			      }
    			    },
    			    error: function() {
    			    $("#preberiSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka: 'Neznan EHR ID'");
    			    }
  					});
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			      if (najdu == false){
    			        $("#kreirajVisino").val(res[0].height);
    			        $("#kreirajEhrId").val(ehrId);
    			        //$("#kreirajEhrId").val($("#preberiEHRid").val());
    			        //$('#preberiPredlogoBolnika').trigger('change');
    			        ehr4 = ehrId;
    			        istiEHR = ehrId;
    			        dodej();
    			      }
    			      //console.log(res[0].height);
    			    },
    			    error: function() {
    			    	$("#preberiSporocilo").html("<span class='obvestilo label " +
                "label-danger fade-in'>Napaka: 'Neznan EHR ID'");
    			    }
  					});
  		},
  		error: function() {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka: 'Neznan EHR ID'");
          //JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}

function dodej() {
  if ($("#kreirajEhrId").val() == istiEHR){
    console.log("halo?");
    var x = document.getElementById("preberiPredlogoBolnika");
    var option = document.createElement("option");
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datum = $("#kreirajDatumRojstva").val();
    var visina = $("#kreirajVisino").val();
    var teza = $("#kreirajTezo").val();
    var ImePriimek = ime + " " + priimek;
    
    if ($("#kreirajEhrId").val()==""){
      $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" + "Najprej vstavite osebne podatke, nato generirajte EHR ID, z gumbom <strong>Generiranje podatkov</strong>'");
    }
    
    else if (!ime || !priimek || !datum || !visina || !teza || ime =="" || priimek=="" || datum=="" || visina=="" || teza==""){
      $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" + "Pravilno vstavite podatke'");
    }
    else{
      $("#kreirajEhrId").val(ehr4);
      option.text = ImePriimek;
      option.value = ime + "," + priimek + "," + datum + "," + visina + "," + teza + "," + ehr4;
      x.add(option, x[stevec]);
      document.getElementById("preberiPredlogoBolnika").options[stevec].selected = 'selected';
      var element = document.getElementById("preberiPredlogoBolnika");
      element.classList.remove("mystyle");
      stevec++;
      $("#kreirajSporocilo").html("");
      var element2 = document.getElementById("Gumb");
      element2.classList.add("button");
      $('#preberiPredlogoBolnika').trigger('change');
      istiEHR ="";
      }
    }
    else {
      $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" + "EHR ID se ne ujema z EHR ID-jem osebe.'");
    }
  }
  
  function ITM(){
    
    
    var vis3 = $("#kreirajVisino").val();
    var vis = ($("#kreirajVisino").val()/100);
    var vis2 = vis*vis;
    var tez = $("#kreirajTezo").val();
    var BMI = tez/vis2;
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var podatki = $("#kreirajDatumRojstva").val().split("-");
    var today = new Date();
    var dd = today.getDate()
    var mm = today.getMonth() + 1
    var yyyy = today.getFullYear();
    
    var meja1 = 19;
    var meja2 = 24;
    var meja3 = 29;
    var meja4 = 39;

    if (mm>podatki[1]){
       var age = yyyy - podatki[0];
    }
    else if (mm==podatki[1] && dd>=podatki[2]){
      var age = yyyy - podatki[0];
    }
    else {
      var age = yyyy - podatki[0] - 1;
    }
    
    
    if (checked){
      var spol = "moškega"
      var odmikHeight = vis3-180.27;
      var odmikWeight = tez-82.44;
      meja1 += 1;
      meja2 += 1;
      meja3 += 1;
      meja4 += 1;
    }
    else{
      var spol = "ženskega"
      var odmikHeight = vis3 - 167.41;
      var odmikWeight = tez-67.53;
    }
    
    
   if (age>65){
       meja1 += 5;
       meja2 += 5;
       meja3 += 5;
       meja4 += 5;
    }
    else if (age>55){
       meja1 += 4;
       meja2 += 4;
       meja3 += 4;
       meja4 += 4;
    }
    else if (age>45){
       meja1 += 3;
       meja2 += 3;
       meja3 += 3;
       meja4 += 3;
    }
    else if (age>35){
       meja1 += 2;
       meja2 += 2;
       meja3 += 2;
       meja4 += 2;
    }
    else if (age>25){
       meja1 += 1;
       meja2 += 1;
       meja3 += 1;
       meja4 += 1;
    }
    
    
    
    if (Math.abs(odmikWeight)<10){
      odmikWeight = parseFloat(odmikWeight).toFixed(2);
    }
    else{
      odmikWeight = parseFloat(odmikWeight).toFixed(1);
    }
    
    if(odmikWeight>=0){
      document.getElementById("vrednostAverageWeight").innerHTML = "+" + odmikWeight;
    }
    else if (odmikWeight<0){
      document.getElementById("vrednostAverageWeight").innerHTML = odmikWeight;
    }
    
    if(BMI>=10){
      BMI = parseFloat(BMI).toFixed(1);
    }
    else if(BMI<10){
      BMI = parseFloat(BMI).toFixed(2);
    }
    document.getElementById("vrednostITM").innerHTML = BMI;
    
    if (Math.abs(odmikHeight)<10){
      odmikHeight = parseFloat(odmikHeight).toFixed(2);
    }
    else{
      odmikHeight = parseFloat(odmikHeight).toFixed(1);
    }
    
    if(odmikHeight>=0){
      document.getElementById("vrednostAverageHeight").innerHTML = "+" + odmikHeight;
    }
    else if (odmikHeight<0){
      document.getElementById("vrednostAverageHeight").innerHTML =  odmikHeight;
    }
    
    if(BMI<meja1){
     var msg = "premajhno težo";
    }
    else if (BMI<meja2){
      var msg = "normalno težo";
    }
    else if (BMI<meja3){
      var msg = "prekomerno težo";
    }
    else if (BMI<meja4){
      var msg = "adipoznost";
    }
    else if (BMI>=meja4){
      var msg = "hudo adipoznost"
    }
    
    
    
    if (document.getElementById("Nov").selected || document.getElementById("nic").selected){
      document.getElementById("vrednostITM").innerHTML = parseFloat(0,00).toFixed(2);
      document.getElementById("vrednostAverageHeight").innerHTML = "+" + parseFloat(0,00).toFixed(2);
      document.getElementById("vrednostAverageWeight").innerHTML = "+" + parseFloat(0,00).toFixed(2);
      $("#msg").html("");
    }
    else{
      $("#msg").html(
      "<div class='alert alert-info ITM'>" +
      "Oseba " + ime + " " + priimek + ", " + spol +" spola" + ", s starostjo " + age + " ima ITM " + BMI + " kar kaže na " + msg + "." +
      "</div>"    
      )
    }
  }
  
  
  
function graf() {
  
  if (document.getElementById("Nov").selected || document.getElementById("nic").selected){
    var barva="#ff7b25";
  }
  else {
    var barva="yellow";
  }
  
  var ime = $("#kreirajIme").val();
  var priimek = $("#kreirajPriimek").val();
  
  var itm = $("#vrednostITM").html();
  itm = parseFloat(itm);
  console.log(itm+1);
  var podatki = $("#kreirajDatumRojstva").val().split("-");
  var today = new Date();
  var dd = today.getDate()
  var mm = today.getMonth() + 1
  var yyyy = today.getFullYear(); 
  
  if (mm>podatki[1]){
       var age = yyyy - podatki[0];
  }
    else if (mm==podatki[1] && dd>=podatki[2]){
      var age = yyyy - podatki[0];
  }
    else {
      var age = yyyy - podatki[0] - 1;
  }
  
  if (age>65){
    var ix=5;
  }
  else if (age>55){
    var ix=4;
  }
  else if(age>45){
    var ix=3;
  }
  else if (age>35){
    var ix=2;
  }
  else if(age>25){
    var ix=1;
  }
  else if (age>=18){
    var ix=0;
  }
  
  console.log(ix);
  
  var meja1 = 19;
  var meja2 = 24;
  var meja3 = 29;
  var meja4 = 39;
  
  if(checked){
  meja1 = 20;
  meja2 = 25;
  meja3 = 30;
  meja4 = 40;
  var spolol = "moški";
  }
  else {
    var spolol = "ženski";
  }

var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	animationEnabled: true,
	title: {
		text: "ITM glede na starost za " + spolol + " spol"
	},
	axisY: {
		title: "ITM",
	},
	axisX:{
	  valueFormatString:" ",
	  title: "Starost",
	},
	legend: {
		horizontalAlign: "right", // "center" , "right"
    verticalAlign: "center",  // "top" , "bottom"
    fontSize:15,
	},
	dataPointWidth: 60,
	
	
	data: [{
		type: "rangeColumn",
		//showInLegend: true,
		//toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}",
		dataPoints: [		
			{x:0, label: "18-24", y: [0, meja1], name:"Premajhna teza", color:"#ff7b25", toolTipContent: "<div style=color:'#ff7b25'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}", },
			{x:0, label: "18-24", y: [meja1, meja2], color:"royalblue", name:"Normalna teza", toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:0, label: "18-24", y: [meja2, meja3], color:"#c83349", name:"Prekomerna teza", toolTipContent: "<div style=color:'#c83349'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:0, label: "18-24", y: [meja3, meja4], color:"#622569", name:"Adipoznost", toolTipContent: "<div style=color:'#622569'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:0, label: "18-24", y: [meja4, 60], color:"#82b74b", name:"Huda adipoznost", toolTipContent: "<div style=color:'#82b74b'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}"},
			
			{x:1, label: "25-34", y: [0, meja1+1] , color:"#ff7b25", name:"Premajhna teza", toolTipContent: "<div style=color:'#ff7b25'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}"},
			{x:1, label: "25-34", y: [meja1+1, meja2+1], color:"royalblue", name:"Normalna teza", toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:1, label: "25-34", y: [meja2+1, meja3+1], color:"#c83349", name:"Prekomerna teza", toolTipContent: "<div style=color:'#c83349'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:1, label: "25-34", y: [meja3+1, meja4+1], color:"#622569", name:"Adipoznost", toolTipContent: "<div style=color:'#622569'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:1, label: "25-34", y: [meja4+1, 61], color:"#82b74b", name:"Huda adipoznost", toolTipContent: "<div style=color:'#82b74b'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			
			{x:2, label: "35-44", y: [0, meja1+2] , color:"#ff7b25", name:"Premajhna teza", toolTipContent: "<div style=color:'#ff7b25'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}"},
			{x:2, label: "35-44", y: [meja1+2, meja2+2], color:"royalblue", name:"Normalna teza", toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:2, label: "35-44", y: [meja2+2, meja3+2], color:"#c83349", name:"Prekomerna teza", toolTipContent: "<div style=color:'#c83349'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:2, label: "35-44", y: [meja3+2, meja4+2], color:"#622569", name:"Adipoznost", toolTipContent: "<div style=color:'#622569'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:2, label: "35-44", y: [meja4+2, 62], color:"#82b74b", name:"Huda adipoznost", toolTipContent: "<div style=color:'#82b74b'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			
			{x:3, label: "45-54", y: [0, meja1+3], color:"#ff7b25", name:"Premajhna teza", toolTipContent: "<div style=color:'#ff7b25'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:3, label: "45-54", y: [meja1+3, meja2+3], color:"royalblue", name:"Normalna teza", toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:3, label: "45-54", y: [meja2+3, meja3+3], color:"#c83349", name:"Prekomerna teza", toolTipContent: "<div style=color:'#c83349'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:3, label: "45-54", y: [meja3+3, meja4+3], color:"#622569", name:"Adipoznost", toolTipContent: "<div style=color:'#622569'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:3, label: "45-54", y: [meja4+3, 63], color:"#82b74b", name:"Huda adipoznost", toolTipContent: "<div style=color:'#82b74b'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			
			{x:4, label: "55-64", y: [0, meja1+4], color:"#ff7b25", name:"Premajhna teza", toolTipContent: "<div style=color:'#ff7b25'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:4, label: "55-64", y: [meja1+4, meja2+4], color:"royalblue", name:"Normalna teza", toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:4, label: "55-64", y: [meja2+4, meja3+4], color:"#c83349", name:"Prekomerna teza", toolTipContent: "<div style=color:'#c83349'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:4, label: "55-64", y: [meja3+4, meja4+4], color:"#622569", name:"Adipoznost", toolTipContent: "<div style=color:'#622569'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:4, label: "55-64", y: [meja4+4, 64], color:"#82b74b", name:"Huda adipoznost", toolTipContent: "<div style=color:'#82b74b'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			
			{x:5, label: "65+", y: [0, meja1+5], color:"#ff7b25", name:"Premajhna teza", toolTipContent: "<div style=color:'#ff7b25'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:5, label: "65+", y: [meja1+5, meja2+5], color:"royalblue", name:"Normalna teza", toolTipContent: "<div style=color:'royalblue'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:5, label: "65+", y: [meja2+5, meja3+5], color:"#c83349", name:"Prekomerna teza", toolTipContent: "<div style=color:'#c83349'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}"},
			{x:5, label: "65+", y: [meja3+5, meja4+5], color:"#622569", name:"Adipoznost", toolTipContent: "<div style=color:'#622569'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
			{x:5, label: "65+", y: [meja4+5, 65], color:"#82b74b", name:"Huda adipoznost", toolTipContent: "<div style=color:'#82b74b'>"+ "{name}" + "</div>" + "Starost: {label}<br>Zgornja meja ITM: {y[1]}<br>Spordnja meja ITM: {y[0]}" },
		]
	},
	{
	  type:"scatter",
	  toolTipContent: null,
	  markersize:8,
	  color:"#82b74b",
	  showInLegend:true,
	  legendText: "Huda Adipoznost",
	  dataPoints:[{x:0, y:0}]
	},
		{
	  type:"scatter",
	  toolTipContent: null,
	  markersize:8,
	  color:"#622569",
	  showInLegend:true,
	  legendText: "Adipoznost",
	  dataPoints:[{x:0, y:0}]
	},
		{
	  type:"scatter",
	  toolTipContent: null,
	  markersize:8,
	  color:"#c83349",
	  showInLegend:true,
	 legendText: "Prekomerna teža",
	  dataPoints:[{x:0, y:0}]
	},
		{
	  type:"scatter",
	  toolTipContent: null,
	  markersize:8,
	  
	  color:"royalblue",
	  showInLegend:true,
	  legendText: "Normalna teža",
	  dataPoints:[{x:0, y:0}]
	},
	{
	  type:"scatter",
	  toolTipContent: null,
	  markersize:8,
	  
	  color:"#ff7b25",
	  showInLegend:true,
	  legendText: "Premajhna teža",
	  dataPoints:[{x:0, y:0}]
	},
		{
	  type:"scatter",
	  markersize:8,
	  showInLegend:true,
	  color:"yellow",
	  legendText: "ITM od osebe",
	  dataPoints:[{x:ix, y:itm, color:barva, toolTipContent:"ITM osebe " + ime + " " + priimek + " je " + itm }]
	}
	

	]
});
chart.render();

}

window.onload = function () {
  graf();
}

function posljiTweet(){
    var ITM = $("#vrednostITM").html();
    if(ITM>=20 && ITM <=25){
      var zadovoljen = "Mislim, da si lahko malo privoščim, "
    }
    else if(ITM<20){
      var zadovoljen = "Mislim, da moram začet jesti, "
    }
    else if (ITM<33){
      var zadovoljen = "Mislim, da moram obiskati fitnes kdaj pa kdaj, "
    }
    else{
      var zadovoljen = "Mislim, da moram k zdravniku, "
    }
    var url = "https://twitter.com/intent/tweet";
    var hashtags = "ITM, takingcare, loveyourself";
    var via="username";
    window.open(url+"?text="+ zadovoljen + "saj je moj ITM " + ITM +". Pa tvoj? Izmeri svoj ITM na super enostavni strani: https://ois-matemato.c9users.io/matemato.bitbucket.org/index.html"+";hashtags="+hashtags+";via="+via,"","width=800,height=300");
    //window.open("https://www.facebook.com/sharer/sharer.php?u=hey");
   // window.open("https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2Fshare-button%2F&display=popup&ref=plugin&src=like&kid_directed_site=0&app_id=113869198637480")
    //window.open("https://www.facebook.com/sharer/sharer.php?u=https://matemato.bitbucket.io/index.html&ref=plugin&src=like&kid_directed_site=0");
    //window.open("https://api.twitter.com/1.1/search/tweets.json");
}

function sheraj(){
    window.open("https://www.facebook.com/sharer/sharer.php?u=https://matemato.bitbucket.io/index.html&display=popup", "facebook", "width=500,height=300");
}

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
  
  
  
  window.addEventListener("load", function(){
  
  function pridobiBolnice(callback){
      var xobj = new XMLHttpRequest();
		  xobj.overrideMimeType("application/json");
			xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
			xobj.onreadystatechange = function()
			{
				 if (xobj.readyState == 4 && xobj.status == "200") {
        			var json = JSON.parse(xobj.responseText);
        			callback(json);
				 }
			}
			xobj.send(null);
  }
  
  function Bolnice(json){
    var podatki = json.features;
    for(var i = 0; i < podatki.length; i++){
      if(podatki[i].geometry.type == "Polygon"){
        if(podatki[i].geometry.coordinates.length == 2){
          var latlngs_1=[];
          for(var j = 0; j < podatki[i].geometry.coordinates[0].length; j++){
            var tmp = [podatki[i].geometry.coordinates[0][j][1],podatki[i].geometry.coordinates[0][j][0]];
            latlngs_1.push(tmp);
          }
          var latlngs_2=[];
          for(var j = 0; j < podatki[i].geometry.coordinates[1].length; j++){
            var tmp = [podatki[i].geometry.coordinates[1][j][1],podatki[i].geometry.coordinates[1][j][0]];
            latlngs_2.push(tmp);
          }
          var polygon = L.polygon([latlngs_1,latlngs_2],{color: 'blue'});
          if(podatki[i].properties.name != undefined && podatki[i].properties["addr:street"] != undefined && podatki[i].properties["addr:housenumber"] != undefined)
            polygon.bindPopup(podatki[i].properties.name + "<br/>" + podatki[i].properties["addr:street"] + " " + podatki[i].properties["addr:housenumber"]).addTo(map);
          else
            polygon.bindPopup("Ni podatkov!").addTo(map);
          map.addLayer(polygon);
          layers.push(polygon);
          bolnice.push(podatki[i]);
        }
        else{
          var latlngs = [];
          for(var j = 0; j < podatki[i].geometry.coordinates[0].length; j++){
            var tmp = [podatki[i].geometry.coordinates[0][j][1],podatki[i].geometry.coordinates[0][j][0]];
            latlngs.push(tmp);
          }
          var polygon = L.polygon(latlngs,{color: 'blue'});
          if(podatki[i].properties.name != undefined && podatki[i].properties["addr:street"] != undefined && podatki[i].properties["addr:housenumber"] != undefined)
            polygon.bindPopup(podatki[i].properties.name + "<br/>" + podatki[i].properties["addr:street"] + " " + podatki[i].properties["addr:housenumber"]).addTo(map);
          else
            polygon.bindPopup("Ni podatkov!").addTo(map);
          map.addLayer(polygon);
          layers.push(polygon);
          bolnice.push(podatki[i]);
        }
      }
    }
  }
  
  function klikNaBolnico(e){
    var latlng = e.latlng;
    for(var i = 0; i < layers.length; i++){
      var tmp = layers[i].getBounds().getCenter();
      if(distance(latlng["lat"],latlng["lng"],tmp["lat"],tmp["lng"],"K") < 3)
        layers[i].setStyle({color:'green'});
      else
        layers[i].setStyle({color:'blue'});
    }
  }
  
  var mapOptions = {
    center: [46.05004, 14.46931],
    zoom: 12
  };
  map = new L.map('mapa',mapOptions);
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  map.addLayer(layer);
  pridobiBolnice(Bolnice);
  
  map.on("click",klikNaBolnico);
  
});